#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "cc_misc.h"
#include "main.h"

comp_dict_t *dict_create(unsigned int size, comp_dict_t *previousScope) {
	comp_dict_t *hashtable = NULL;

	if (size < 1){
		return NULL;
	}

	// Aloca o dicionário
	if ((hashtable = (comp_dict_t*) malloc(sizeof(comp_dict_t))) == NULL) {
		return NULL;
	}

	// Aloca espaço para as entradas da hashtable
	if ((hashtable->table = (comp_dict_entry_t **) malloc(sizeof(comp_dict_entry_t *) * size)) == NULL) {
		return NULL;
	}

	// Inicializa os ponteiros da tabela como NULL
	unsigned int i;
	for (i = 0; i < size; i++) {
		hashtable->table[i] = NULL;
	}

	hashtable->previousScope = previousScope;

	if (previousScope == NULL) {
		hashtable->level = 0;
		hashtable->deslocamento = 0;
	}
	else {
		hashtable->level = previousScope->level+1;
		hashtable->deslocamento = 0;//previousScope->deslocamento;
	}

	hashtable->size = size;

	return hashtable;
}

// Algoritmo de hash djb2
unsigned long hash(unsigned char *str)
{
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c;

    return hash;
}

unsigned int dict_hash_func(char *key, unsigned int size){
    // Transforma a key em um inteiro entre 0 e o tamanho do dicionario
	unsigned int num = hash(key) % size;
	return num;
}

comp_dict_value_t* dict_create_new_value(char *value, unsigned int line, int type){
	
	comp_dict_value_t *new = malloc(sizeof(comp_dict_value_t));
	new->line = line;
	new->type = type;
	
	switch(type){
    	case SIMBOLO_LITERAL_FLOAT: ;
			float *floatVal;
			floatVal = (float*) malloc(sizeof(float));
			*floatVal = atof(value);
			new->convertedValue = floatVal;
      		break;

    	case SIMBOLO_LITERAL_INT: ;
			int *intVal;
			intVal = (int*) malloc(sizeof(int));
			*intVal =  atoi(value);
			new->convertedValue = intVal;
			break;

		case SIMBOLO_LITERAL_CHAR: ;
			char *charVal;
			charVal = (char*) malloc(sizeof(char));
			*charVal = *value;
			new->convertedValue = charVal;
			break;

		case SIMBOLO_LITERAL_STRING:
			new->convertedValue = value;
			break;

		case SIMBOLO_LITERAL_BOOL: ;
			short *boolVal;
			boolVal = (short*) malloc(sizeof(short));
			if ( strcmp(value, "true") )
				*boolVal = 0;
			else 
				*boolVal = 1;
			new->convertedValue = boolVal;
			break;

		case SIMBOLO_IDENTIFICADOR:
			new->convertedValue = NULL;
			break;
	}

	return new;

}

comp_dict_entry_t* dict_insert(comp_dict_t *dictionary, char *key, comp_dict_value_t *value) {

	if (dictionary == NULL || key == NULL){
		return NULL;
	}

	unsigned int hashNum = dict_hash_func(key, dictionary->size);
	comp_dict_entry_t *entry;
	comp_dict_entry_t *lastEntry = NULL;
	entry = dictionary->table[hashNum];

	if (entry == NULL){
	    // Se esta entrada do dicionario ainda não foi inicializada, insere o primeiro item
		entry = (comp_dict_entry_t *) malloc(sizeof(comp_dict_entry_t));
		entry->key = key;
		entry->line = value->line;
		entry->type = value->type;
		entry->convertedValue = value->convertedValue;
		entry->lista_args = NULL; //adicionando a lista de argumentos
		entry->next = NULL;
		entry->flag_global = 0;
		dictionary->table[hashNum] = entry;
		return entry;
	}
	else{
		while (entry) {
			// Se a key já existe e é do mesmo tipo modifica o valor			
			
			if (strcmp(key, entry->key) == 0 && value->type == entry->type) {
				entry->line = value->line;
				entry->type = value->type;

				free(key);
				free(entry->convertedValue);

				entry->convertedValue = value->convertedValue;
				return entry;
			}
			lastEntry = entry;
			entry = entry->next;
		}

		// Se não existe insere um novo no fim da lista
		comp_dict_entry_t *newPair;
		newPair = (comp_dict_entry_t *) malloc(sizeof(comp_dict_entry_t));
		newPair->key = key;
		newPair->line = value->line;
		newPair->type = value->type;
		newPair->convertedValue = value->convertedValue;
		newPair->next = NULL;
		newPair->flag_global = 0;

		lastEntry->next = newPair;
		return newPair;
	}	
}

comp_dict_entry_t* get_local_identifier(comp_dict_t *dictionary, char *key) {

	if (dictionary == NULL || key == NULL){
		return NULL;
	}

	unsigned int hashNum = dict_hash_func(key, dictionary->size);
	comp_dict_entry_t *entry;
	entry = dictionary->table[hashNum];

	if (entry == NULL)
		return 0;
	else {
		while (entry) {
			// Se a key já existe e é do mesmo tipo já foi declarado			
			if (strcmp(key, entry->key) == 0 && SIMBOLO_IDENTIFICADOR == entry->type) {
				return entry;
			}
			entry = entry->next;
		}
		return NULL;
	}
}

comp_dict_entry_t* get_identifier(comp_dict_t *dictionary, char *key) {

	if (dictionary == NULL || key == NULL){
		return NULL;
	}	

	//printf("\nIs defined?? key %s\n",key);	

	unsigned int hashNum = dict_hash_func(key, dictionary->size);
	comp_dict_entry_t *entry;

	do {	
		//dict_print(dictionary);
		entry = dictionary->table[hashNum];

		if (entry != NULL) {
			//printf("	Entry não é null\n");
			while (entry) {
				// Se a key já existe e é do mesmo tipo já foi declarado			
				if (strcmp(key, entry->key) == 0 && SIMBOLO_IDENTIFICADOR == entry->type) {
					//printf("	Encontrei a key\n");
					return entry;
				}
				entry = entry->next;
			}
		}
		else
			//printf("	Entry é null\n");

		//printf("	Vou buscar no pai\n");
		
		dictionary = dictionary->previousScope;
	} while (dictionary != NULL);

	//printf("	Não encontrei a key\n\n");

	return NULL;
}

comp_dict_entry_t* retrieve_entry(comp_dict_t *dictionary, char *key) {

	if (dictionary == NULL || key == NULL){
		return NULL;
	}	

	unsigned int hashNum = dict_hash_func(key, dictionary->size);
	comp_dict_entry_t *entry;

	do {	
		entry = dictionary->table[hashNum];

		if (entry != NULL) {
			while (entry) {
				// Se a key já existe e é do mesmo tipo já foi declarado			
				if (strcmp(key, entry->key) == 0 && SIMBOLO_IDENTIFICADOR == entry->type) {
					return entry;
				}
				entry = entry->next;
			}
		}

		dictionary = dictionary->previousScope;
	} while (dictionary != NULL);

	return NULL;
}

int get_id_size(int type){
	switch(type){
		case IKS_INT:	return 4; 
		break;
		case IKS_FLOAT:	return 8; 
		break;
		case IKS_BOOL:	return 1; 
		break;
		case IKS_CHAR:	return 1; 
		break;
		case IKS_STRING:return 1; 
		break;
	}
}

void free_Scope(comp_dict_t *dictionary){

	comp_dict_entry_t *entry; 
	comp_dict_entry_t *temp;

	dict_print(dictionary);

	unsigned int i;
	for (i = 0; i < dictionary->size; i++){
		entry = dictionary->table[i];
		if (entry == NULL)
			free(dictionary->table[i]);

		while (entry != NULL){
			temp = entry;
			entry = entry->next;
			free(temp->key);
			if (temp->type != SIMBOLO_IDENTIFICADOR && temp->type != SIMBOLO_LITERAL_STRING){
					free(temp->convertedValue);
			}

			//limpa_args(temp->lista_args);
			free(temp);
		}
		//free(dictionary->table[i]);
	}

	free(dictionary->table);
	free(dictionary);
}

int limpa_args(comp_dict_args_t* lista){
	comp_dict_args_t *lista_args;
	comp_dict_args_t *temp;

	lista_args = lista;
	while(lista != NULL){
		temp = lista_args;
		lista_args = lista_args->next;
		free(temp);
	}
	lista_args = NULL;

	return 0;
}

int dict_print(comp_dict_t *dictionary){
	
	/*printf("\n\n Printando Dicionario\n");
	
	unsigned int i;
	for (i = 0; i < dictionary->size; i++){
		comp_dict_entry_t *entry = dictionary->table[i];
		
		while (entry != NULL){
			printf("Key: %s - linha %d\n", entry->key, entry->line);
			entry = entry->next;
		}
	}
	
	printf("\n");*/
	return 0;
}

//adiciona parametros
comp_dict_args_t* novo_arg(int n_arg){
    comp_dict_args_t *temp_arg = (comp_dict_args_t*)malloc(sizeof(comp_dict_args_t));
    
     if(temp_arg != NULL){
        temp_arg->t_arg = n_arg;
        //printf("arguinho: %d\n", n_arg);
        temp_arg->next = NULL;
        temp_arg->tac = NULL;
    }

    return temp_arg ;
}

//adiciona o ultimo argumento
comp_dict_args_t* link_arg(comp_dict_args_t* lst, comp_dict_args_t* arg){
    lst->next = arg;

    return lst;
}

int compara_lista_args(comp_dict_entry_t *entrada, comp_tree_t *lista_expr){

    comp_dict_args_t *params = entrada->lista_args;
	comp_tree_t *exp = lista_expr;
	
	int i = 0;
	
	while(params!=NULL && exp!=NULL){
		if(params->t_arg != exp->entrada->type){
		    //printf("t_arg: %d, iks: %d", params->t_arg, exp->entrada->type);    
			return IKS_ERROR_WRONG_TYPE_ARGS;
		}
		params = params->next;
		exp = exp->filhos[i];
		i++;
	}
	
	if(params==NULL && lista_expr!=NULL){
		return IKS_ERROR_EXCESS_ARGS;
	}
	else if(params!=NULL && lista_expr==NULL){
	    return IKS_ERROR_MISSING_ARGS;	
	}
	else
  		return 0;
}







