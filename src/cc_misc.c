#include "cc_misc.h"
#include "cc_tree.h"
#include "cc_dict.h"
#include "cc_list.h"
#include "cc_gv.h"

comp_dict_t *dictionary;
comp_dict_args_t *lista_returns;
extern comp_tree_t *arvore;
extern tree_list *lista_de_nodos;


int getLineNumber (void)
{
	return yylineno;
}

void yyerror (char const *mensagem)
{
  fprintf (stderr, "%s\n", mensagem); //altere para que apareça a linha
}

void main_init (int argc, char **argv)
{
  //implemente esta função com rotinas de inicialização, se necessário
	dictionary = dict_create(100, NULL);
	lista_returns = (comp_dict_args_t*) malloc(sizeof(comp_dict_args_t));
	lista_returns->t_arg = -1;
	lista_returns->next = NULL;
	lista_de_nodos = (tree_list*) malloc(sizeof(tree_list));
	lista_de_nodos->node = NULL;
	lista_de_nodos->previous = NULL;
;}

void main_finalize (void)
{
	//printf("\n\n\ OEEE \n\n");
    //implemente esta função com rotinas de inicialização, se necessário
    //dict_print(dictionary);
  
}
