#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "cc_tree.h"
#include "main.h"


comp_tree_t* novo_no(int novo_tipo, comp_dict_entry_t *nova_entrada, int novo_nfilhos, comp_tree_t* novo_filho, ...) {
    //nova estrutura
    comp_tree_t *novo_nodo = malloc(sizeof(comp_tree_t));
    //aloca os filhos, como no máximo teremos 4 filhos.
    comp_tree_t **novos_filhos = malloc(sizeof(comp_tree_t)* 4);
    
    novo_nodo->tipo = novo_tipo;
    novo_nodo->coercao = 0;
    novo_nodo->entrada = nova_entrada;
    novo_nodo->n_filhos = novo_nfilhos;
    novo_nodo->tac = malloc(sizeof(tac_t));
    novo_nodo->filhos = NULL;

    if (nova_entrada != NULL) 
        novo_nodo->iks_type = nova_entrada->iks_type;
    else
        novo_nodo->iks_type = NONE;
    
    //gv_declare
    /*if(nova_entrada != NULL) {  
		gv_declare(novo_tipo, novo_nodo, nova_entrada->key);
    }
    else {
		gv_declare(novo_tipo, novo_nodo, NULL);		
    } */     
    
    if (novo_nfilhos > 0) {
        //estrutura para mais de um filho.
        va_list lista_filhos;
        //temporário para a lista de filhos
        comp_tree_t *temp;
        
        va_start(lista_filhos, novo_filho); 
        
        int i;
        for(temp = novo_filho, i = 0; i < novo_nfilhos; temp = va_arg(lista_filhos, comp_tree_t*), i++) {
            novos_filhos[i] = temp;
            //gv_connect(novo_nodo, novos_filhos[i]);
        } 
        va_end(lista_filhos); 
         
    } 
    
    novo_nodo->filhos = novos_filhos;
 
    return novo_nodo;
}

comp_tree_t* add_filho(comp_tree_t* pai, comp_tree_t* filho) {    
    if(pai != NULL  && filho != NULL){ //se ninguem for null
        pai->filhos[pai->n_filhos] = filho;
        //gv_connect(pai, filho);
	    pai->n_filhos++;
    }
    else if(pai != NULL)
        return pai;
    else
        return filho;

    //caso seja null, apenas retorna o pai, sem adicionar nenhum filho        
    return pai;
}

void deleta_no(comp_tree_t* no) {
    int i;
    for(i=0; i < no->n_filhos; i++) {
        if(no->filhos[i] != NULL) {
            /*First it removes the sons nodes.*/
            deleta_no(no->filhos[i]);
        }
    }

    /*and then, it removes the "father" initial node*/
    deleta_no(no);
    no = NULL;
}





