#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cc_tree.h"
#include "cc_list.h"
#include "cc_misc.h"
#include "main.h"

//valor maximo do tamanho de um registrador.
#define STR_REGISTRADOR 100

int n_registradores = 0;
int n_rotulos = 0;


/*cria o tac a partir de um tipo específico de tac*/
tac_t* novo_tac(int novo_tipo, char* novo_r, char* novo_op1, char* novo_op2) {
    tac_t* novo_tac = (tac_t*)malloc(sizeof(tac_t));
    
    novo_tac->tipo = novo_tipo;
    novo_tac->r = novo_r;
	novo_tac->op1 = novo_op1;
	novo_tac->op2 = novo_op2;
	novo_tac->prev = NULL;
	
	return novo_tac;
}

char* novo_registrador() {
    char *reg = (char*)malloc(STR_REGISTRADOR*sizeof(char));
    sprintf(reg,"r%d", n_registradores);
	n_registradores++;

	return reg;
}

char *novo_rotulo() {

	char *rotulo = (char*)malloc(STR_REGISTRADOR*sizeof(char));
    sprintf(rotulo,"l%d", n_rotulos);
	n_rotulos++;

	return rotulo;
}

/*Concatena um tac em outro*/
tac_t* tac_concat(tac_t* t1, tac_t* t2) {
    tac_t* temp = t1;
    
    while(temp->prev != NULL) {
        temp = temp->prev;
    }
    
    temp->prev = t2;
    
    return t1;
}

tac_t* inverte_tac(tac_t* tac) {
    //ponteiro temporário para a lista
    tac_t* temp;
    //ponteiro para a lista reversa
    tac_t* rev = NULL;
    while(tac != NULL) {
        temp = tac->prev;
        tac->prev = rev;
        rev = tac;
        tac = temp;
    }
    
    return rev;
}

/*2.inicia os registradores fp, sp e rbss*/
tac_t* inicia_regs() {
    char* r_fp = (char*)malloc(STR_REGISTRADOR*sizeof(char));
    strcpy(r_fp, "fp");
    
    char* r_rbss = (char*)malloc(STR_REGISTRADOR*sizeof(char));
	strcpy(r_rbss, "rbss");
	
	char* r_sp = (char*)malloc(STR_REGISTRADOR*sizeof(char));
	strcpy(r_sp, "sp");
	
	tac_t* tac_fp = novo_tac(loadI, "0", r_fp, NULL);
	tac_t* tac_sp = novo_tac(loadI, "0", r_sp, NULL);
	tac_t* tac_rbss = novo_tac(loadI, "0", r_rbss, NULL);
	tac_t* tac_main = novo_tac(mainI, "main", NULL, NULL);
	
	tac_main = tac_concat(tac_main, tac_rbss);
	tac_main = tac_concat(tac_main, tac_sp);
	tac_main = tac_concat(tac_main, tac_fp);
	
	return tac_main;
}


//para expressões
tac_t *tac_exp(int tipo, tac_t* exp1, tac_t* exp2) {

    //printf("tipo: %d, exp1: %d, exp2: %d\n",tipo, exp1->tipo, exp1->tipo);
    
    //novo registrador
    //char *reg = novo_rotulo();
    char *reg = novo_registrador();

	tac_t *exp = novo_tac(tipo, reg, exp1->r, exp2->r);
	
	//agora tem de adicionar os join da lista
	exp = tac_concat(exp, exp2);
	exp = tac_concat(exp, exp1);
	
	return exp;
}

tac_t* tac_lit(int tipo, comp_dict_entry_t* lit) {

    //printf("tipo: %d, lit: %d\n",tipo, lit->iks_type);
    //printf("IKS_INT: %d, IKS_FLOAT: %d, IKS_CHAR: %d\n", IKS_INT, IKS_FLOAT, IKS_CHAR);

    char *reg = novo_registrador();
    
    //salva o tipo de literal associado, 15 pois nunca vai ser necessário mais que 15x o tamanho de char.
    char *tipo_lit = (char*)malloc(15*sizeof(char));
    
    //if(lit->iks_type == IKS_INT) {
    if(tipo == IKS_INT) {
		int *data_int =  (int *) lit->convertedValue;
		//passando o int para char*
		sprintf(tipo_lit, "%d", (*data_int));
		//printf("tipo_lit: %s\n", tipo_lit);
	}     
	//else if(lit->iks_type == IKS_FLOAT) {
	else if(tipo == IKS_FLOAT) {
		float *data_float =  (float *) lit->convertedValue;
	    sprintf(tipo_lit, "%f", (*data_float));
	    //printf("tipo_lit: %s\n", tipo_lit);
	}
	//else if(lit->iks_type == IKS_CHAR) {
	else if(tipo == IKS_CHAR) {
	    char *data_char =  (char *) lit->convertedValue;
		tipo_lit = data_char;
		//printf("tipo_lit: %s\n", tipo_lit);
	}
	
    //printf("\n\n Literal: tipo: %d, reg: %s tipo_lit: %s\n",tipo, reg, tipo_lit);

    //return novo_tac(tipo, reg, tipo_lit, NULL);
    return novo_tac(loadI, reg, tipo_lit, NULL);
}

tac_t* tac_ident(int tipo, comp_dict_entry_t* ident) {
    
    //printf("tipo: %d, ident: %d\n",tipo, ident->iks_type);    
    
    char *regMem = novo_registrador();
    char *reg = novo_registrador();    
	char *reg_type = (char*)malloc(5*sizeof(char));
	char *desloc = (char*)malloc(STR_REGISTRADOR*sizeof(char));
	
	//se a variavel é global, vai no registrador rbss
	if(ident->flag_global == 1) 
		strcpy(reg_type,"rbss");
    //caso seja local, usa o registrador fp(rarp)
	else 
		strcpy(reg_type,"rarp");

	sprintf(desloc,"%d",ident->deslocamento);

    tac_t *tacMem = novo_tac(ADDI, regMem, reg_type, desloc);

    // No TAC do identificador, reg é o conteúdo que ele guarda e op1 é o endereço da memória já calculado
    tac_t* identf = novo_tac(tipo, reg, regMem, desloc);
    identf = tac_concat(identf, tacMem);

	return identf;
}

tac_t* tac_vetor(comp_dict_entry_t* ident, comp_dict_args_t* lista) {

    //printf("tipo: %d, ident: %d\n",tipo, ident->iks_type);    
    
    char *regMem = novo_registrador();
    char *reg = novo_registrador();
    char *reg_type = (char*)malloc(5*sizeof(char));
    char *desloc = (char*)malloc(STR_REGISTRADOR*sizeof(char));

    // Verifica o tamanho do tipo do array
    char *tipo_size = (char*)malloc(15*sizeof(char));
    int data_int =  get_id_size(ident->iks_type);
    sprintf(tipo_size, "%d", data_int);

    // Verifica escopo e descolamento
    if(ident->flag_global == 1) 
        strcpy(reg_type,"rbss"); //se a variavel é global, vai no registrador rbss    
    else 
        strcpy(reg_type,"rarp"); //caso seja local, usa o registrador fp(rarp)
    
    sprintf(desloc,"%d",ident->deslocamento);
    
    /*
    comp_dict_args_t* params = ident->lista_args;
    while(params != NULL){
        //printf("%d ", params->t_arg);
        params = params->next;
    }*/    
     
    // Calculo do deslocamento interno, dentro do vetor
    char *regV = novo_registrador();
    tac_t *deslInterno = novo_tac(MULTI, regV, lista->tac->r, tipo_size);

    // Calculo do endereço de memória da posição acessada.
    tac_t *tacMem1 = novo_tac(ADDI, regMem, regV, desloc); // Deslocamento interno + deslocamento externo
    tac_t *tacMem2 = novo_tac(AST_ARIM_SOMA, regMem, regMem, reg_type); // Soma os delocamentos no escopo correto

    //return novo_tac(tipo, reg, reg_type, desloc);
    tac_t* vetor = novo_tac(AST_IDENTIFICADOR, reg, regMem, desloc);
    vetor = tac_concat(vetor, tacMem2);
    vetor = tac_concat(vetor, tacMem1);
    vetor = tac_concat(vetor, deslInterno);
    vetor = tac_concat(vetor, lista->tac);
    //vetor = tac_concat(vetor, d);
    return vetor;
}

tac_t* tac_relop(int tipo, tac_t* exp1, tac_t* exp2) {
    //printf("tipo: %d, exp1: %d, exp2: %d\n",tipo, exp1->tipo, exp1->tipo);
    
    if(tipo == AST_LOGICO_E){
		tac_t* t_and = novo_tac(LABEL_TRUE, novo_rotulo(), NULL, NULL);
		
		exp1->t = t_and->r;
		exp2 = tac_concat(exp2, t_and);
		exp2 = tac_concat(exp2, exp1);
		
		return exp2;
	}

	if(tipo == AST_LOGICO_OU){
		tac_t* t_or = novo_tac(LABEL_FALSE, novo_rotulo(), NULL, NULL);

		exp1->f = t_or->r;		
		exp2 = tac_concat(exp2, t_or);
		exp2 = tac_concat(exp2, exp1);

		return exp2;
	}

	if(tipo == AST_LOGICO_COMP_NEGACAO){
		tac_t* exp = novo_tac(tipo, NULL, NULL, NULL);

		exp1 = tac_concat(exp1, exp);

		return exp1;

	}
       
    //para expressões lógicas que não precisam de curto circuito.
    tac_t *relop = novo_tac(tipo, exp1->r, exp1->r, exp2->r);

    relop = tac_concat(relop, exp2);
    relop = tac_concat(relop, exp1);

    return relop;
    //return novo_tac(tipo, exp1->r, exp1->r, exp2->r);
}

tac_t* tac_if(tac_t* expr, tac_t* then) {

    //printf("exp: %d, then: %d\n", exp->tipo, then->tipo);    
        
    //Caso seja true e caso apenas siga
    char* l_true = novo_rotulo();
    char* seq = novo_rotulo();
    
    tac_t* l_then = novo_tac(LABEL_TRUE, l_true, NULL, NULL);
    tac_t* end = novo_tac(END, seq, NULL, NULL);
    
    tac_t* if_then = novo_tac(AST_IF_ELSE, NULL, NULL, NULL);
    if_then->t = l_true;
	if_then->f = seq;
	//cbr r1 -> l2, l3 | PC = l2 se r1 = true, senão PC = l3
	tac_t *cbr = novo_tac(CBR, expr->r, if_then->t, if_then->f); 
	
	if_then = tac_concat(if_then, end);
	if_then = tac_concat(if_then, then);
	if_then = tac_concat(if_then, l_then);
	if_then = tac_concat(if_then, cbr);
	if_then = tac_concat(if_then, expr);

	return end;   
}

tac_t* tac_if_else(tac_t* expr, tac_t* then, tac_t* _else) {

    char* l_true = novo_rotulo();
	char* l_false = novo_rotulo();
	char* seq = novo_rotulo();
	
	tac_t* l_then = novo_tac(LABEL_TRUE, l_true, NULL, NULL);
	tac_t* l_else = novo_tac(LABEL_FALSE, l_false, NULL, NULL);
	tac_t* end = novo_tac(END, seq, NULL, NULL);
	tac_t* _goto = novo_tac(GOTO, seq, NULL, NULL);

	tac_t* if_then = novo_tac(AST_IF_ELSE, NULL, NULL, NULL);
	if_then->t = l_true;
	if_then->f = l_false;

	tac_t* cbr = novo_tac(CBR, expr->r, if_then->t, if_then->f);

	end = tac_concat(end, _else);
	end = tac_concat(end, l_else);
	end = tac_concat(end, _goto);
	end = tac_concat(end, then);
	end = tac_concat(end, l_then);
	end = tac_concat(end, cbr);
	end = tac_concat(end, expr);
	end = tac_concat(end, if_then);

	return end;
}

tac_t* tac_while_do(tac_t* exp, tac_t* command) {
    char* laco = novo_rotulo();
    char* seq = novo_rotulo();
    char* jmp = novo_rotulo();
    
    tac_t* t_laco = novo_tac(LABEL_TRUE, laco, NULL, NULL);
	tac_t* t_goto = novo_tac(GOTO, laco, NULL, NULL);
	tac_t* t_jmp = novo_tac(LABEL_TRUE, jmp, NULL, NULL);
	tac_t* t_seq = novo_tac(END, seq, NULL, NULL);
	
	tac_t* t_whiledo = novo_tac(AST_WHILE_DO, NULL, NULL, NULL);
	t_whiledo->t = jmp;
	t_whiledo->f = seq;
	
	tac_t* cbr = novo_tac(CBR, exp->r, jmp, seq);
	
	t_seq = tac_concat(t_seq, t_goto);
	t_seq = tac_concat(t_seq, command);
	t_seq = tac_concat(t_seq, t_jmp);
	t_seq = tac_concat(t_seq, cbr);
	t_seq = tac_concat(t_seq, exp);
	t_seq = tac_concat(t_seq, t_laco);
	t_seq = tac_concat(t_seq, t_whiledo);
	
	return t_seq;
}

tac_t* tac_do_while(tac_t* command, tac_t* exp) {

    char* laco = novo_rotulo();
    char* seq = novo_rotulo();
    
    tac_t* t_laco = novo_tac(LABEL_TRUE, laco, NULL, NULL);
    tac_t* t_seq = novo_tac(END, seq, NULL, NULL);
    
    tac_t* do_while = novo_tac(AST_DO_WHILE, NULL, NULL, NULL); 
    do_while->t = laco;
	do_while->f = seq;

    tac_t *cbr = novo_tac(CBR, exp->r, do_while->t, do_while->f); 
	
    t_seq = tac_concat(t_seq, cbr);
	t_seq = tac_concat(t_seq, exp);
	t_seq = tac_concat(t_seq, command);
	t_seq = tac_concat(t_seq, t_laco);
	//t_seq = tac_concat(t_seq, do_while);
	
    return t_seq;
}

tac_t* tac_atr(int tipo, tac_t* dest, tac_t* src) {
    //printf("tipo: %d, dest: %d, src:  %d \n",tipo, dest->tipo, src->tipo);
    //tac_t* atr = novo_tac(tipo, dest->op1, src->r, dest->op2);
    tac_t* atr = novo_tac(tipo, NULL, dest->op1, src->r);

    atr = tac_concat(atr, src);
    atr = tac_concat(atr, dest);
	return atr;
}

tac_t* tac_shift(int tipo, tac_t* ident, tac_t* lit) {

    //printf("\n%s\n",lit->r);
    tac_t* shift = novo_tac(tipo, ident->r, ident->op1, lit->r); 
    
    shift = tac_concat(shift, lit);
    shift = tac_concat(shift, ident);
    return shift;
}

tac_t* add_tac_decl_fun(comp_dict_entry_t *ident, tac_t* corpo) {

}

void gera_iloc(tac_t* main_tac) {
    tac_t* temp = main_tac;
    //printf("gera_iloc");
    while(temp != NULL) {
        char* output = (char*)malloc(100*sizeof(char));
        switch(temp->tipo) {
            case mainI: //inicializa o programa na main
                sprintf(output,"jumpI -> %s\n", temp->r);
                printf("%s", output);
            break;
			case loadI: //inicializa registradores sp, fp e rbss
				sprintf(output, "loadI %s -> %s\n", temp->r, temp->op1);
				printf("%s", output);
			break;
            case AST_ATRIBUICAO:
                //esse store tem de ser arrumado
                //sprintf(output,"storeAI %s => %s, %s\n", temp->op1, temp->r, temp->op2);
                sprintf(output,"store %s => %s\n", temp->op2, temp->op1);
                printf("%s", output);
            break;
            case AST_ARIM_MULTIPLICACAO:
                sprintf(output,"mult %s, %s => %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_ARIM_SOMA:
                sprintf(output,"add %s, %s => %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_ARIM_SUBTRACAO:
                sprintf(output,"sub %s, %s => %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_ARIM_DIVISAO:
                sprintf(output,"div %s, %s => %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_LOGICO_COMP_DIF:
                sprintf(output,"cmp_NE %s,%s -> %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_LOGICO_COMP_IGUAL:
                sprintf(output,"cmp_EQ %s,%s -> %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_LOGICO_COMP_LE:
                sprintf(output,"cmp_LE %s,%s -> %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_LOGICO_COMP_GE:
                sprintf(output,"cmp_GE %s,%s -> %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_LOGICO_COMP_L:
                sprintf(output,"cmp_LT %s,%s -> %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_LOGICO_COMP_G:
                sprintf(output,"cmp_GT %s,%s -> %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_IDENTIFICADOR:
                sprintf(output,"load %s => %s\n", temp->op1, temp->r);
                //sprintf(output,"loadAI %s, %s => %s\n", temp->op1, temp->op2, temp->r);
                //sprintf(output,"addI %s, %s => %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case LABEL:
                sprintf(output,"%s: \n", temp->r);
                printf("%s", output);
            break;
            case LABEL_TRUE:
                sprintf(output,"%s: nop\n", temp->r);
                printf("%s", output);
            break;
            case LABEL_FALSE:
                sprintf(output,"%s: nop\n", temp->r);
                printf("%s", output);
            break;
            case END:
                sprintf(output,"%s: nop\n", temp->r);
                printf("%s", output);
            break;            
            case CBR: 
                sprintf(output,"cbr %s -> %s,%s\n", temp->r, temp->op1, temp->op2);
                printf("%s", output);
			break;
			case GOTO:
                sprintf(output,"jumpI -> %s\n", temp->r);
                printf("%s", output);
            break;
            case MULTI: 
                sprintf(output,"multI %s, %s => %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case ADDI:
                sprintf(output,"addI %s, %s => %s\n", temp->op1, temp->op2, temp->r);
                printf("%s", output);
            break;
            case AST_SHIFT_LEFT:
                sprintf(output,"lshift %s, %s => %s\n", temp->r, temp->op2, temp->r);                
                printf("%s", output);
                sprintf(output,"store %s => %s\n", temp->r, temp->op1);
                printf("%s", output);
            break;
            case AST_SHIFT_RIGHT:
                sprintf(output,"rshift %s, %s => %s\n", temp->r, temp->op2, temp->r);
                printf("%s", output);
                sprintf(output,"store %s => %s\n", temp->r, temp->op1);
                printf("%s", output);
            break;
        }
        temp = temp->prev;
        free(output);
    }
}

//Funções do registro de ativação

ra_pilha_t *init_pilha(){
    ra_pilha_t *pilha = malloc(sizeof(ra_pilha_t));
    pilha->top = NULL;
    return pilha;
}

ra_pilha_t *add_ra(ra_pilha_t *pilha, ra_t *nova_ra){
    if (pilha->top == NULL) {
        nova_ra->prev = NULL;
        pilha->top = nova_ra;
    } 
    else { 
        nova_ra->prev = pilha->top;
        pilha->top = nova_ra;
    }
    
    return pilha; 
}

ra_t *init_ra(){
    ra_t* nova_ra = malloc(sizeof(ra_t));
    nova_ra->prev = NULL;
    nova_ra->var_locais = NULL;
    nova_ra->parametros = NULL;
    return nova_ra; 
}

ra_t *create_ra(ra_t *nova_ra, variavel_ra_t *locais, variavel_ra_t *params){
    nova_ra->var_locais = locais;
    nova_ra->parametros = params;
    return nova_ra;
}

variavel_ra_t *add_var(variavel_ra_t *lista, char *identificador, int deslocamento){
    variavel_ra_t *var = malloc(sizeof(variavel_ra_t));
    variavel_ra_t *it = lista;
    var->identificador = identificador;
    var->deslocamento = deslocamento;
    var->next = NULL;
            
    if (lista == NULL) {
        lista = var;     
    } 
    else {
        while(it->next != NULL){
            it = it->next;
        }
        it->next = var;
    }

    return lista;
}
