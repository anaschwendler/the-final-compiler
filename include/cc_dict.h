#ifndef __COMP_DICT_H
#define __COMP_DICT_H

//lista de argumentos, necessária para levantar erros de quantidade de argumentos

struct comp_dict_args_s {
    int t_arg;
    struct comp_dict_args_s *next;
    struct tac_t *tac;
};

typedef struct comp_dict_args_s comp_dict_args_t;

// Conteudo da entrada do dicionario
struct comp_dict_value_s {
	int line;
	int type;
	void *convertedValue;
};

typedef struct comp_dict_value_s comp_dict_value_t;

// Entrada no dicionário
struct comp_dict_entry_s {
	char *key;
	int line;
	int type;
	int size;
	int id_type;  // Identificador simples, vetor ou nome de função
	int iks_type; // No caso de ser um identificador, diz se é int, float, char etc
	int flag_global; //para adição de tac, quando é identificador tem dois registradores diferentes
	int deslocamento; //para adição de tac, salva o deslocamento de um identificador
	void *convertedValue;
	comp_dict_args_t *lista_args; /* Lista de argumentos, caso exista. */
	struct comp_dict_entry_s *next;
};

typedef struct comp_dict_entry_s comp_dict_entry_t;

// Dicionário
struct comp_dict_s{
	unsigned int size;
	int deslocamento;
  	int level; // Global scope = 0
	struct comp_dict_entry_s **table;
	struct comp_dict_s *previousScope;
};

typedef struct comp_dict_s comp_dict_t;

// Criação/inicialização do dicionário
comp_dict_t *dict_create(unsigned int size, comp_dict_t *previousScope);

// Criação de um novo conteudo de entrada do dicionario
comp_dict_value_t* dict_create_new_value(char *value, unsigned int line, int type);

// Função Hash
unsigned int dict_hash_func(char *key, unsigned int size);

// Inserção no dicionário
comp_dict_entry_t* dict_insert(comp_dict_t *dictionary, char *key, comp_dict_value_t *value);

// Verifica se identificador já está declarado no escopo recebido
comp_dict_entry_t* get_local_identifier(comp_dict_t *dictionary, char *key);

// Verifica se identificador já está declarado em algum escopo válido
comp_dict_entry_t* get_identifier(comp_dict_t *dictionary, char *key);

// Recupera a entrada no dicionário
comp_dict_entry_t* retrieve_entry(comp_dict_t *dictionary, char *key);

// Retorna o tamanho ocupado pela variavel
int get_id_size(int type);

// Libera a memória das estruturas de um dicionário
void free_Scope(comp_dict_t *dictionary);

// Printa o dicionário
int dict_print(comp_dict_t *dictionary);

// Adiciona argumentos a lista de argumentos
comp_dict_args_t* novo_arg(int arg);

// Adiciona o ultimo argumento
comp_dict_args_t* link_arg(comp_dict_args_t* lst, comp_dict_args_t* arg);

// Compara o numero de argumentos, retorna erros
//int compara_lista_args(comp_dict_entry_t *entrada, comp_tree_t *lista_expr);

#endif
