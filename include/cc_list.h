#ifndef __CC_LIST_H
#define __CC_LIST_H

#include "cc_misc.h"

#define LABEL_TRUE 40
#define GOTO 41
#define END 43
#define LABEL_FALSE 44
#define CBR 45
#define MULTI 46
#define ADDI 47
#define LABEL 48

#define loadI 1
#define mainI 2

typedef struct tac_t {
    int tipo;
    char *r, *op1, *op2; //três endereços
	char* t; //true
	char* f; //false
    struct tac_t *prev;
}tac_t;

//inverte a lista - por que como montamos nosso tac bottom up, precisamos inverter a lista
tac_t* inverte_tac(tac_t* tac);

//cria o tac a partir de um tipo específico de tac
tac_t* novo_tac(int tipo, char* novo_r, char* novo_op1, char* novo_op2);

/*Concatena um tac em outro*/
tac_t* tac_concat(tac_t* t1, tac_t* t2);

//inicializa os registradores
tac_t* inicia_regs();

//para expressões
tac_t *tac_exp(int tipo, tac_t* exp1, tac_t* exp2);

/*Ensaios de funções a serem implementadas num futuro próximo*/
//para literais
tac_t* tac_lit(int tipo, comp_dict_entry_t* lit1);

//para identificadores
tac_t* tac_ident(int tipo, comp_dict_entry_t* ident1);

//operações lógicas
tac_t* tac_relop(int tipo, tac_t* exp1, tac_t* exp2);

//if do tipo exp1 - expressão, exp2 - comando
tac_t* tac_if(tac_t* expr, tac_t* then);

//if else, do tipo exp1 - expressão, exp2 - comando, exp3 - else
tac_t* tac_if_else(tac_t* expr, tac_t* then, tac_t* _else);

//while do, do tipo exp1 - expressao, exp2 - comando
tac_t* tac_while_do(tac_t* exp, tac_t* command); 

//do_while, do tipoexp1 - comando, exp2 - expressão
tac_t* tac_do_while(tac_t* command, tac_t* exp);

//atribuição
tac_t* tac_atr(int tipo, tac_t* dest, tac_t* src);

//declaração de vetor
tac_t* tac_vetor(comp_dict_entry_t* ident, comp_dict_args_t* lista);

//para shift
tac_t* tac_shift(int tipo, tac_t* ident, tac_t* lit);

tac_t* add_tac_decl_fun(comp_dict_entry_t *ident, tac_t* corpo);

//

typedef struct variavel_ra_t{
	char *identificador;
	int deslocamento;
	struct variavel_ra_t *next;
}variavel_ra_t;

typedef struct ra_t{
	variavel_ra_t *var_locais;
	variavel_ra_t *parametros;
	variavel_ra_t *retorno;
	struct ra_t *prev;
}ra_t;

typedef struct ra_pilha_t {
	ra_t *top;
}ra_pilha_t;

ra_t *init_ra();
ra_t *create_ra(ra_t *nova_ra, variavel_ra_t *locais, variavel_ra_t *params);
ra_pilha_t *init_pilha();
ra_pilha_t *add_ra(ra_pilha_t *pilha, ra_t *nova_ra);
variavel_ra_t *add_var(variavel_ra_t *lista, char *identificador, int deslocamento);

#endif
