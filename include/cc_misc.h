#ifndef __MISC_H
#define __MISC_H
#include <stdio.h>
#include "cc_dict.h"
#include "cc_tree.h"
#include "cc_ast.h"
#include "errors.h"

extern comp_dict_t *dictionary;
extern comp_dict_args_t *lista_returns;
extern int yylineno;

int getLineNumber (void);
void yyerror (char const *mensagem);
void main_init (int argc, char **argv);
void main_finalize (void);

#endif
