#ifndef __COMP_TREE_H
#define __COMP_TREE_H

#include <stdlib.h>
#include "cc_misc.h"

typedef struct comp_tree_t {
    int tipo; /* de cc_ast.h */
    int coercao; /* recebe o tipo definido pela coerção*/
    int iks_type; // No caso de ser um identificador, diz se é int, float, char etc
    comp_dict_entry_t *entrada; /* entradas com ponteiro para a tabela de símbolos */
    int n_filhos; /* quantidade de filhos */
    struct tac_t *tac;
    struct comp_tree_t **filhos; /* caso o no tenha mais de um filho, teremos um vetor de filhos */
}comp_tree_t;

typedef struct tree_list {
    struct comp_tree_t *node;
    struct tree_list *previous;
}tree_list;

comp_tree_t* novo_no(int novo_tipo, comp_dict_entry_t* nova_entrada, int novo_nfilhos, comp_tree_t* filho, ...);
comp_tree_t* add_filho(comp_tree_t* pai, comp_tree_t* filho);
void deleta_no(comp_tree_t* no);

#endif
