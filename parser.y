/*
	The Final Compiler Team
	Ana Schwendler
	Lucas Cardozo
*/

%{
#include "main.h"
#include "cc_misc.h"
#include <stdio.h>

comp_tree_t* arvore = NULL;
tree_list *lista_de_nodos = NULL;
int vector_size = 0;

%}

%union {
	comp_dict_entry_t *valor_simbolo_lexico;
	comp_dict_args_t *lst_args;
	comp_dict_args_t *lista_vetor;
	char* identificador;
	comp_tree_t *nodo;
	int type;
	int tam_vetor;
}

/* Declaração dos tokens da linguagem */
%token<type> TK_PR_INT
%token<type> TK_PR_FLOAT
%token<type> TK_PR_BOOL
%token<type> TK_PR_CHAR
%token<type> TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_STATIC
%token TK_PR_CONST
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR
%token<valor_simbolo_lexico> TK_LIT_INT
%token<valor_simbolo_lexico> TK_LIT_FLOAT
%token<valor_simbolo_lexico> TK_LIT_FALSE
%token<valor_simbolo_lexico> TK_LIT_TRUE
%token<valor_simbolo_lexico> TK_LIT_CHAR
%token<valor_simbolo_lexico> TK_LIT_STRING
%token<identificador> TK_IDENTIFICADOR
%token TOKEN_ERRO


/* definindo tipos para as estruturas da gramática*/

%type<nodo> programa
%type<nodo> inicio
%type<nodo> decl_funcao
%type<nodo> sequencia_comandos
%type<nodo> comando_com_vazio
%type<nodo> comando
%type<nodo> retorno
%type<nodo> expressao
%type<nodo> identificador
%type<nodo> vetor_indexado
%type<nodo> literal
%type<nodo> chamada_funcao
%type<nodo> shift
%type<nodo> lista_expressao_nao_vazia
%type<nodo> fluxo_controle
%type<nodo> bloco_comandos
%type<nodo> bloco_func
%type<nodo> entrada
%type<nodo> saida
%type<nodo> atribuicao
%type<type> tipo
%type<type>tipo_ident
%type <lista_vetor> lista_inteiros
%type <lista_vetor> lista_expressao_vetor

/* lista de parâmetros */
%type<lst_args>param
%type<lst_args>params_f

/* gera erros mais significativos */
%error-verbose 

/* remoção de conflitos (ambiguidades) */
%right TK_PR_THEN TK_PR_ELSE
%left '+' '-'
%left '*' '/'
%left TK_OC_OR
%left TK_OC_AND
%left TK_OC_LE TK_OC_GE TK_OC_EQ TK_OC_NE '<' '>'
%left '!'
%right INVERSAO

%%
/* Regras (e ações) da gramática */

/*Conjunto opcional de variáveis globais e um conjunto opcional de funções, terminadas por ;*/

programa:
    /*Adicionado uma regra inicio, para facilitar inicio da construção árvore.*/ 
    inicio { 
        if($1 == NULL)
            $$ = novo_no(AST_PROGRAMA, NULL, 0, NULL);
        else
            $$ = novo_no(AST_PROGRAMA, NULL, 1, $1);
        arvore = $$;
        
        tac_t* prog = inicia_regs();
        $1->tac = tac_concat($1->tac, prog);
        //$$->tac = $1->tac;
        
        if ($1 != NULL){
        	$$->tac = inverte_tac($1->tac);
        	gera_iloc($$->tac); 
        }       
    }
	|  { 
		 $$ = novo_no(AST_PROGRAMA, NULL, 0, NULL);
		 arvore = $$;
		}
;

inicio: 
    decl_global inicio { 
    	$$ = $2; 
    	$$->tac = $2->tac;
    }
	| decl_funcao inicio {
	    //printf("decl_funcao\n");
	    //se o programa é vazio, o nodo é igual ao nodo do inicio do programa. 
	    if($2 == NULL)
	        $$ = $1;
	    else {
	        $$ = add_filho($1, $2);
	        $$->tac = tac_concat($2->tac,$1->tac);
	    }
	}
	| decl_global { 
		$$ = NULL; 
	}
	| decl_funcao { 
		$$ = $1; 
		$$->tac = $1->tac;
	}
;

/* 2.1 variáveis globais - variavel ou vetor */
decl_global: 
	decl_var_global ';'
	| decl_vetor ';'
;

/* 2.2 funções - cabeçalho + bloco_comandos */
/*cabeçalho foi eliminado, pois dava conflito com gv_declare*/
decl_funcao: 
    tipo TK_IDENTIFICADOR '(' ')' {    
    	comp_dict_entry_t *entry = get_local_identifier(dictionary, $2);
		if (entry != NULL){
				//printf("Identificador ja declarado");
				return IKS_ERROR_DECLARED;
		}
		else {	

			// Retorno
			comp_dict_args_t *newRet;
	    	newRet = (comp_dict_args_t*) malloc(sizeof(comp_dict_args_t));
			newRet->t_arg = $1;
			newRet->next = lista_returns;
			lista_returns = newRet;			

			comp_dict_entry_t *dict_entry; 
			comp_dict_value_t *newValue;
			newValue = dict_create_new_value($2, yylineno, SIMBOLO_IDENTIFICADOR);
			dict_entry = dict_insert(dictionary, $2, newValue);
			dict_entry->id_type = ID_FUNCAO;
			dict_entry->iks_type = $1;
			dict_entry->lista_args = NULL;
			dict_entry->size = get_id_size($1);
			if (dictionary->level == 0) 
				dict_entry->flag_global = 1;
			dict_entry->deslocamento = dictionary->deslocamento;
			dictionary->deslocamento += dict_entry->size;
			yylval.valor_simbolo_lexico = dict_entry;
			free(newValue);
			//dict_print(dictionary);
		}
		comp_dict_t *newScope = dict_create(100, dictionary); 
        dictionary = newScope;
	} 
	bloco_func { 
		comp_dict_entry_t *dict_entry;
		dict_entry = retrieve_entry(dictionary, $2);
        if($6 == NULL) {
	        $$ = novo_no(AST_FUNCAO, dict_entry, 0, NULL);
	    }
	    else {
	        $$ = novo_no(AST_FUNCAO, dict_entry, 1, $6);
	        $$->tac = $6->tac;
	    } 

	    comp_dict_entry_t *entry = get_local_identifier(dictionary, $2);
	    char* label_func = $2;
	    tac_t* define_label = novo_tac(LABEL, label_func, NULL, NULL);
       // $$->tac = $6->tac;
        $$->tac = tac_concat($$->tac, define_label);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    comp_dict_t *oldScope = dictionary;
	    dictionary = dictionary->previousScope;
	    free_Scope(oldScope);

	    comp_dict_args_t *oldRet = lista_returns;
	    lista_returns = lista_returns->next;
	    free(oldRet);
    }
	| TK_PR_STATIC tipo TK_IDENTIFICADOR '(' ')' {		
		comp_dict_entry_t *entry = get_local_identifier(dictionary, $3);
		if (entry != NULL){
				//printf("Identificador ja declarado");
				return IKS_ERROR_DECLARED;
		}
		else {

			// Retorno
			comp_dict_args_t *newRet;
	    	newRet = (comp_dict_args_t*) malloc(sizeof(comp_dict_args_t));
			newRet->t_arg = $2;
			newRet->next = lista_returns;
			lista_returns = newRet;	

			comp_dict_entry_t *dict_entry; 
			comp_dict_value_t *newValue;
			newValue = dict_create_new_value($3, yylineno, SIMBOLO_IDENTIFICADOR);
			dict_entry = dict_insert(dictionary, $3, newValue);
			dict_entry->id_type = ID_FUNCAO;
			dict_entry->iks_type = $2;
			dict_entry->size = get_id_size($2);
			if (dictionary->level == 0) 
				dict_entry->flag_global = 1;
			dict_entry->deslocamento = dictionary->deslocamento;
			dictionary->deslocamento += dict_entry->size;
			yylval.valor_simbolo_lexico = dict_entry;
			free(newValue);
			//dict_print(dictionary);
		}
		comp_dict_t *newScope = dict_create(100, dictionary); 
        dictionary = newScope;
	} 
	bloco_func { 
		comp_dict_entry_t *dict_entry;
		dict_entry = retrieve_entry(dictionary, $3);
	    if($7 == NULL) {
	        $$ = novo_no(AST_FUNCAO, dict_entry, 0, NULL);
	    }
	    else {
	        $$ = novo_no(AST_FUNCAO, dict_entry, 1, $7);
	        $$->tac = $7->tac;
	    } 

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;
	    
	    comp_dict_t *oldScope = dictionary;
	    dictionary = dictionary->previousScope;
	    free_Scope(oldScope);

	    comp_dict_args_t *oldRet = lista_returns;
	    lista_returns = lista_returns->next;
	    free(oldRet);
	}
	| tipo TK_IDENTIFICADOR '(' {		
		comp_dict_entry_t *entry = get_local_identifier(dictionary, $2);
		if (entry != NULL){
				//printf("Identificador ja declarado");
				return IKS_ERROR_DECLARED;
		}
		else {

			// Retorno
			comp_dict_args_t *newRet;
	    	newRet = (comp_dict_args_t*) malloc(sizeof(comp_dict_args_t));
			newRet->t_arg = $1;
			newRet->next = lista_returns;
			lista_returns = newRet;	

			comp_dict_entry_t *dict_entry; 
			comp_dict_value_t *newValue;
			newValue = dict_create_new_value($2, yylineno, SIMBOLO_IDENTIFICADOR);
			dict_entry = dict_insert(dictionary, $2, newValue);
			dict_entry->id_type = ID_FUNCAO;
			dict_entry->iks_type = $1;
			dict_entry->size = get_id_size($1);
			if (dictionary->level == 0) 
				dict_entry->flag_global = 1;
			dict_entry->deslocamento = dictionary->deslocamento;
			dictionary->deslocamento += dict_entry->size;
			yylval.valor_simbolo_lexico = dict_entry;
			free(newValue);
			//dict_print(dictionary);
		}
		comp_dict_t *newScope = dict_create(100, dictionary); 
        dictionary = newScope;
	}
	params_f ')' bloco_func { 
		comp_dict_entry_t *dict_entry;
		dict_entry = retrieve_entry(dictionary, $2);
	    if($7 == NULL) {
	        $$ = novo_no(AST_FUNCAO, dict_entry, 0, NULL);
	    }
	    else {
	        $$ = novo_no(AST_FUNCAO, dict_entry, 1, $7);
	        $$->tac = $7->tac;
	    } 
	    
		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    comp_dict_t *oldScope = dictionary;
	    dictionary = dictionary->previousScope;
	    dict_entry->lista_args = $5;
	    free_Scope(oldScope);	

	    comp_dict_args_t *oldRet = lista_returns;
	    lista_returns = lista_returns->next;
	    free(oldRet);
	}
	| TK_PR_STATIC tipo TK_IDENTIFICADOR '(' {		
		comp_dict_entry_t *entry = get_local_identifier(dictionary, $3);
		if (entry != NULL){
				//printf("Identificador ja declarado");
				return IKS_ERROR_DECLARED;
		}
		else {

			// Retorno
			comp_dict_args_t *newRet;
	    	newRet = (comp_dict_args_t*) malloc(sizeof(comp_dict_args_t));
			newRet->t_arg = $2;
			newRet->next = lista_returns;
			lista_returns = newRet;	

			comp_dict_entry_t *dict_entry; 
			comp_dict_value_t *newValue;
			newValue = dict_create_new_value($3, yylineno, SIMBOLO_IDENTIFICADOR);
			dict_entry = dict_insert(dictionary, $3, newValue);
			dict_entry->id_type = ID_FUNCAO;
			dict_entry->iks_type = $2;
			dict_entry->size = get_id_size($2);
			if (dictionary->level == 0) 
				dict_entry->flag_global = 1;
			dict_entry->deslocamento = dictionary->deslocamento;
			dictionary->deslocamento += dict_entry->size;
			yylval.valor_simbolo_lexico = dict_entry;
			free(newValue);
			//dict_print(dictionary);
		}
		comp_dict_t *newScope = dict_create(100, dictionary); 
        dictionary = newScope;
	}
	params_f ')' bloco_func  { 
		comp_dict_entry_t *dict_entry;
		dict_entry = retrieve_entry(dictionary, $3);
	    if($8 == NULL) {
	        $$ = novo_no(AST_FUNCAO, dict_entry, 0, NULL);
	    }
	    else {
	        $$ = novo_no(AST_FUNCAO, dict_entry, 1, $8);
	        $$->tac = $8->tac;
	    }

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;
		
	    comp_dict_t *oldScope = dictionary;
	    dictionary = dictionary->previousScope;
	    dict_entry->lista_args = $6;
	    free_Scope(oldScope);

	    comp_dict_args_t *oldRet = lista_returns;
	    lista_returns = lista_returns->next;
	    free(oldRet);
	} 

/* 2.3 comandos simples */
tipo: 
	  TK_PR_INT		{ $$ = IKS_INT;    }
	| TK_PR_FLOAT	{ $$ = IKS_FLOAT;  }
	| TK_PR_BOOL	{ $$ = IKS_BOOL;   }
	| TK_PR_CHAR	{ $$ = IKS_CHAR;   }
	| TK_PR_STRING	{ $$ = IKS_STRING; }
;

	/* regras adicionais */
	/* criada, pois sempre se repete*/
tipo_ident: 	
	tipo TK_IDENTIFICADOR {
		comp_dict_entry_t *entry = get_local_identifier(dictionary, $2);
		if (entry != NULL){
			//printf("\nIdentificador já declarado\n");
			return IKS_ERROR_DECLARED;
		} 
		else {
			comp_dict_entry_t *dict_entry; 
			comp_dict_value_t *newValue;
			newValue = dict_create_new_value($2, yylineno, SIMBOLO_IDENTIFICADOR);
			dict_entry = dict_insert(dictionary, $2, newValue);
			dict_entry->id_type = ID_SIMPLES;
			dict_entry->iks_type = $1;
			dict_entry->size = get_id_size($1);
			if (dictionary->level == 0) 
				dict_entry->flag_global = 1;
			dict_entry->deslocamento = dictionary->deslocamento;
			dictionary->deslocamento += dict_entry->size;
			yylval.valor_simbolo_lexico = dict_entry;
			free(newValue);		
			//dict_print(dictionary);
		}
	    $$ = $1;
	}
;

decl_var: 
	tipo_ident 
	| TK_PR_STATIC tipo_ident 
	| TK_PR_CONST tipo_ident
	| TK_PR_STATIC TK_PR_CONST tipo_ident
	| decl_var_inicializada
	| TK_PR_STATIC decl_var_inicializada
	| TK_PR_CONST decl_var_inicializada
	| TK_PR_STATIC TK_PR_CONST decl_var_inicializada
;

decl_var_inicializada:
	tipo_ident TK_OC_LE literal
;

decl_var_global: 
	tipo_ident
	| TK_PR_STATIC tipo_ident
;

decl_vetor: 
	//tipo TK_IDENTIFICADOR '[' TK_LIT_INT ']' {
	tipo TK_IDENTIFICADOR '[' lista_inteiros ']' {
		//printf("\ndecl_vetor\n");
		vector_size = 0;
		comp_dict_entry_t *entry = get_local_identifier(dictionary, $2);
		if (entry != NULL){
			//printf("\nIdentificador já declarado\n");
			return IKS_ERROR_DECLARED;
		} 
		else {
			comp_dict_entry_t *dict_entry; 
			comp_dict_value_t *newValue;
			newValue = dict_create_new_value($2, yylineno, SIMBOLO_IDENTIFICADOR);
			dict_entry = dict_insert(dictionary, $2, newValue);
			dict_entry->id_type = ID_VETOR;
			dict_entry->iks_type = $1;
			//dict_entry->size = get_id_size($1) * $4;
			
			//Calcula tamanho do array em bytes
			comp_dict_args_t* params = $4;
			dict_entry->size = params->t_arg;
			params = params->next;
    		while(params != NULL){
    			dict_entry->size = dict_entry->size * params->t_arg;
        		params = params->next;
    		}
    		dict_entry->size = get_id_size($1) * dict_entry->size;

			dict_entry->lista_args = $4;
			if (dictionary->level == 0) 
				dict_entry->flag_global = 1;
			dict_entry->deslocamento = dictionary->deslocamento;
			dictionary->deslocamento += dict_entry->size;
			yylval.valor_simbolo_lexico = dict_entry;
			free(newValue);		
			//dict_print(dictionary);
		}
	}
	| //TK_PR_STATIC tipo TK_IDENTIFICADOR '[' TK_LIT_INT ']' {
	TK_PR_STATIC tipo TK_IDENTIFICADOR '[' lista_inteiros ']' {
		comp_dict_entry_t *entry = get_local_identifier(dictionary, $3);
		if (entry != NULL){
			//printf("\nIdentificador já declarado\n");
			return IKS_ERROR_DECLARED;
		} 
		else {
			comp_dict_entry_t *dict_entry; 
			comp_dict_value_t *newValue;
			newValue = dict_create_new_value($3, yylineno, SIMBOLO_IDENTIFICADOR);
			dict_entry = dict_insert(dictionary, $3, newValue);
			dict_entry->id_type = ID_VETOR;
			dict_entry->iks_type = $2;
			//int int_lit = atoi($5->convertedValue);
			//dict_entry->size = get_id_size($2)*int_lit;
			dict_entry->lista_args = $5;
			if (dictionary->level == 0) 
				dict_entry->flag_global = 1;
			dict_entry->deslocamento = dictionary->deslocamento;
			dictionary->deslocamento += dict_entry->size;
			yylval.valor_simbolo_lexico = dict_entry;
			free(newValue);		
			//dict_print(dictionary);
		}
	}
;

lista_inteiros:
	TK_LIT_INT ',' lista_inteiros {
		/*int *pt_int = (int *) $1->convertedValue;
		if (vector_size == 0)
			vector_size = *pt_int;
		else
			vector_size = vector_size * (*pt_int);
		$$ = vector_size;*/

		int *pt_int = (int *) $1->convertedValue;
		comp_dict_args_t* ints = novo_arg(*pt_int);
	    $$ = link_arg(ints, $3);
	}
	| TK_LIT_INT { 
		/*int *pt_int = (int *) $1->convertedValue;
		if (vector_size == 0)
			vector_size = *pt_int;
		else
			vector_size = vector_size * (*pt_int);
		$$ = vector_size;*/

		int *pt_int = (int *) $1->convertedValue;
		$$ = novo_arg(*pt_int);
	}
;

params_f: 
	param   { $$ = $1; }
	| param ',' params_f { $$ = link_arg($1, $3); }
;

param: 
	tipo_ident { $$ = novo_arg($1); }
	| TK_PR_CONST tipo_ident { $$ = novo_arg($2); }
;

sequencia_comandos: 
	comando_com_vazio { 
	    if ($1 != NULL) {
	        $$ = $1;	
	        $$->tac = $1->tac;      
	    }
	    else {
	        $$ = NULL;	

	    } 
	}
	| comando_com_vazio ';' sequencia_comandos {	
	    if($3 != NULL){	
	        $$ = add_filho($1, $3);
	         if($1!=NULL && $3!=NULL) {
                $$->tac = tac_concat($3->tac, $1->tac);
            }
            else if( $1 != NULL)
                $$->tac = $1->tac;
            else
                $$->tac = $3->tac;
	    }
	}
;

comando: 
	decl_var { $$ = NULL; }
	| decl_vetor {$$ = NULL; }
	| atribuicao { $$->tac = $1->tac; }
	| bloco_comandos { $$->tac = $1->tac; }
	| entrada { /*printf("entrada\n");*/ }
	| fluxo_controle
	| saida { /*printf("saida\n");*/ }
	| retorno { /*printf("retorno\n");*/ }
	| chamada_funcao { /*printf("funcao\n");*/ }
	| shift { /*printf("shift\n");*/ }
;

comando_com_vazio: 
	comando { $$ = $1; 
		if ($1 != NULL){
			$$->tac = $1->tac;
		}
	}
	//corpo da função é vazio
	| { $$ = NULL; }
;

identificador: 
	TK_IDENTIFICADOR { 
		comp_dict_entry_t *entry = get_identifier(dictionary, $1);
		free($1);
		if (entry == NULL){
			//printf("Identificador não declarado \n\n");
			return IKS_ERROR_UNDECLARED;			
		}
		else{
			$$ = novo_no(AST_IDENTIFICADOR, entry, 0, NULL); 
			$$->iks_type = entry->iks_type;

			tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
	        newListNode->previous = lista_de_nodos;
	        newListNode->node = $$;
	        lista_de_nodos = newListNode;
		}
		$$->tac = tac_ident($$->tipo,entry);
	}
;

vetor_indexado: 
	identificador '[' lista_expressao_vetor ']' {
        //if ($3->iks_type == IKS_STRING) return IKS_ERROR_STRING_TO_X;
		//if ($3->iks_type == IKS_CHAR) return IKS_ERROR_CHAR_TO_X; 
		 
		if ($1->entrada->id_type == ID_SIMPLES) return IKS_ERROR_VARIABLE; 
		if ($1->entrada->id_type == ID_FUNCAO)  return IKS_ERROR_FUNCTION;
		
		//primeiro cria o nodo
		$$ = novo_no(AST_VETOR_INDEXADO, NULL , 2, $1, $3);
		//depois associa o tipo.
		$$->iks_type = $1->entrada->iks_type;

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

        $$->tac = tac_vetor($1->entrada, $3);  
	}
;

lista_expressao_vetor: 
	expressao ',' lista_expressao_vetor {	
		comp_dict_args_t* expr = novo_arg($1->iks_type);
		expr->tac = $1->tac;
	    $$ = link_arg(expr, $3);
	}
	| expressao	{	
    	$$ = novo_arg($1->iks_type);
    	$$->tac = $1->tac;
	}
;

atribuicao: 
	identificador '=' expressao {

		if ($1->entrada->id_type == ID_VETOR)  return IKS_ERROR_VECTOR; 
		if ($1->entrada->id_type == ID_FUNCAO) return IKS_ERROR_FUNCTION;
		
		if ($1->entrada->iks_type != IKS_CHAR && $3->iks_type == IKS_CHAR)     return IKS_ERROR_CHAR_TO_X;
        if ($1->entrada->iks_type != IKS_STRING && $3->iks_type == IKS_STRING) return IKS_ERROR_STRING_TO_X;
        if ($1->entrada->iks_type == IKS_CHAR && $3->iks_type != IKS_CHAR)     return IKS_ERROR_WRONG_TYPE;
        if ($1->entrada->iks_type == IKS_STRING && $3->iks_type != IKS_STRING) return IKS_ERROR_WRONG_TYPE;
        
        //coersão
        if ($1->entrada->iks_type == IKS_INT && $3->iks_type != IKS_INT)       { $3->coercao = X_TO_INT; }
	    if ($1->entrada->iks_type == IKS_FLOAT && $3->iks_type != IKS_FLOAT)   { $3->coercao = X_TO_FLOAT; }
	    if ($1->entrada->iks_type == IKS_BOOL && $3->iks_type != IKS_BOOL)     { $3->coercao = X_TO_BOOL; }
        
        $$ = novo_no(AST_ATRIBUICAO, NULL, 2, $1, $3); 

        tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;
        
        //printf("atribuição!\n");
        $$->tac = tac_atr($$->tipo, $1->tac, $3->tac);

	}
	| vetor_indexado '=' expressao {
			
		//printf("vetor - $1: %d e $3: %d\n", $1->iks_type, $3->iks_type);
	
        if ($1->iks_type != IKS_CHAR && $3->iks_type == IKS_CHAR)     return IKS_ERROR_CHAR_TO_X; 
        if ($1->iks_type != IKS_STRING && $3->iks_type == IKS_STRING) return IKS_ERROR_STRING_TO_X; 
        if ($1->iks_type == IKS_CHAR && $3->iks_type != IKS_CHAR)     return IKS_ERROR_WRONG_TYPE;
        if ($1->iks_type == IKS_STRING && $3->iks_type != IKS_STRING) return IKS_ERROR_WRONG_TYPE;
        
        //coersão
	    if ($1->iks_type == IKS_INT && $3->iks_type != IKS_INT)       { $3->coercao = X_TO_INT; }
	    if ($1->iks_type == IKS_FLOAT && $3->iks_type != IKS_FLOAT)   { $3->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_BOOL && $3->iks_type != IKS_BOOL)     { $3->coercao = X_TO_BOOL; }

        $$ = novo_no(AST_ATRIBUICAO, NULL, 2, $1, $3);
        tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

        //printf("atribuição!\n");
        $$->tac = tac_atr($$->tipo, $1->tac, $3->tac);

	}
;

bloco_comandos: '{' { comp_dict_t *newScope = dict_create(100, dictionary); dictionary = newScope; }
	sequencia_comandos '}' {
    	if ($3 != NULL) {
	    	$$ = novo_no(AST_BLOCO, NULL, 1, $3); 
	    }
	    else {
	        $$ = novo_no(AST_BLOCO, NULL, 0, NULL);
	    }

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    comp_dict_t *oldScope = dictionary;
	    dictionary = dictionary->previousScope;
	    free_Scope(oldScope);

	    $$->tac = $3->tac;
	}
;

bloco_func: 
	'{' sequencia_comandos '}' {
    	$$ = $2;
    	$$->tac = $2->tac;
    }
;

entrada: 
	TK_PR_INPUT expressao '=' '>' expressao { 
		$$ = novo_no(AST_INPUT, NULL, 2, $2, $5); 
		//printf("\nERRO IKS_ERROR_WRONG_PAR_INPUT\n\n");

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

		if ($2->tipo != SIMBOLO_IDENTIFICADOR) return IKS_ERROR_WRONG_PAR_INPUT;
	}
;

saida: 
	TK_PR_OUTPUT lista_expressao_nao_vazia { 
		$$ = novo_no(AST_OUTPUT, NULL, 1, $2); 

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

		if ($2->iks_type != IKS_STRING && $2->tipo != AST_ARIM_SOMA && $2->tipo != AST_ARIM_SUBTRACAO && $2->tipo != AST_ARIM_DIVISAO && $2->tipo != AST_ARIM_INVERSAO) return IKS_ERROR_WRONG_PAR_OUTPUT;
	}
;

retorno: 
	TK_PR_RETURN expressao { 
		//printf("\n\n Ola amiguinho: %d %d", $2->iks_type , lista_returns->t_arg);
		if ( ($2->iks_type == IKS_CHAR|| $2->iks_type == IKS_STRING) && ($2->iks_type != lista_returns->t_arg) ) return IKS_ERROR_WRONG_PAR_RETURN;
		if ( (lista_returns->t_arg == IKS_CHAR|| lista_returns->t_arg == IKS_STRING) && ($2->iks_type != lista_returns->t_arg) ) return IKS_ERROR_WRONG_PAR_RETURN;

		// Caso contrario coercao
		
		$$ = novo_no(AST_RETURN, NULL, 1, $2); 

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;
	}
;

fluxo_controle: 
	TK_PR_IF '(' expressao ')' TK_PR_THEN comando { 
		$$ = novo_no(AST_IF_ELSE, NULL, 2, $3, $6);		    
		$$->tac = tac_if($3->tac, $6->tac); 

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;
	}
	| TK_PR_IF '(' expressao ')' TK_PR_THEN comando TK_PR_ELSE comando { 
		$$ = novo_no(AST_IF_ELSE, NULL, 3, $3, $6, $8);
		if($6->tac == NULL)
		    printf("$6->tac NULL\n");
		$$->tac = tac_if_else($3->tac, $6->tac, $8->tac); 

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;
	}
	| TK_PR_WHILE '(' expressao ')' TK_PR_DO comando { 
		$$ = novo_no(AST_WHILE_DO, NULL, 2, $3, $6); 

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;
        
        
        $$->tac = tac_while_do($3->tac, $6->tac); 
	}
	| TK_PR_DO comando TK_PR_WHILE '(' expressao ')' { 
		$$ = novo_no(AST_DO_WHILE, NULL, 2, $2, $5); 

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

        $$->tac = tac_do_while($2->tac, $5->tac); 
	}
;

chamada_funcao: 
	identificador '(' ')' {		 
	    $$ = novo_no(AST_CHAMADA_DE_FUNCAO, NULL, 1, $1);
		$$->iks_type = $1->entrada->iks_type;

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

		// se der erro retorna o erro esperado.
		int erro = compara_lista_args($1->entrada, NULL);
		if (erro > 0) return erro;
		 
	}
	| identificador '(' lista_expressao_nao_vazia ')' { 
	    $$ = novo_no(AST_CHAMADA_DE_FUNCAO, NULL, 2, $1, $3);	      
		$$->iks_type = $1->entrada->iks_type;

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

		int erro = compara_lista_args($1->entrada, $3);
		if (erro > 0) return erro;  
	}
;

lista_expressao_nao_vazia: 
	expressao { $$ = $1; }
	| expressao ',' lista_expressao_nao_vazia { $$ = add_filho($1, $3); } 
;

literal: 
	TK_LIT_INT { $$->entrada = $1;  }
	| TK_LIT_FLOAT { $$->entrada = $1; }
	| TK_LIT_STRING { $$->entrada = $1; }
	| TK_LIT_CHAR { $$->entrada = $1; }
	| TK_LIT_FALSE { $$->entrada = $1; }
	| TK_LIT_TRUE { $$->entrada = $1; }
;

expressao: 
	identificador 
	| vetor_indexado
	| literal { 
		$$ = novo_no(AST_LITERAL, $1->entrada, 0, NULL); 
		$$->iks_type = $1->entrada->type;
		$$->tac = tac_lit($$->iks_type, $1->entrada); 

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	}
	| '(' expressao ')' { $$ = $2; }
	| expressao '+' expressao { 
	    $$ = novo_no(AST_ARIM_SOMA, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)      return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
        if ($1->iks_type == IKS_INT || $3->iks_type == IKS_FLOAT)     { $1->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_INT)     { $3->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_INT || $3->iks_type == IKS_BOOL)      { $3->coercao = X_TO_INT; }
	    if ($1->iks_type == IKS_BOOL || $3->iks_type == IKS_INT)      { $1->coercao = X_TO_INT; }
	    if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_BOOL)    { $3->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_BOOL || $3->iks_type == IKS_FLOAT)    { $1->coercao = X_TO_FLOAT; }
	    
	    $$->tac = tac_exp(AST_ARIM_SOMA, $1->tac, $3->tac);
	}
	| expressao '-' expressao { 
	    $$ = novo_no(AST_ARIM_SUBTRACAO, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
        if ($1->iks_type == IKS_INT || $3->iks_type == IKS_FLOAT)     { $1->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_INT)     { $3->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_INT || $3->iks_type == IKS_BOOL)      { $3->coercao = X_TO_INT; }
	    if ($1->iks_type == IKS_BOOL || $3->iks_type == IKS_INT)      { $1->coercao = X_TO_INT; }
	    if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_BOOL)    { $3->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_BOOL || $3->iks_type == IKS_FLOAT)    { $1->coercao = X_TO_FLOAT; } 
	    
	    $$->tac = tac_exp(AST_ARIM_SUBTRACAO, $1->tac, $3->tac);
	}
	| expressao '*' expressao { 
	    $$ = novo_no(AST_ARIM_MULTIPLICACAO, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
        if ($1->iks_type == IKS_INT || $3->iks_type == IKS_FLOAT)     { $1->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_INT)     { $3->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_INT || $3->iks_type == IKS_BOOL)      { $3->coercao = X_TO_INT; }
	    if ($1->iks_type == IKS_BOOL || $3->iks_type == IKS_INT)      { $1->coercao = X_TO_INT; }
	    if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_BOOL)    { $3->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_BOOL || $3->iks_type == IKS_FLOAT)    { $1->coercao = X_TO_FLOAT; } 
	    
	    $$->tac = tac_exp(AST_ARIM_MULTIPLICACAO, $1->tac, $3->tac);
	}
	| expressao '/' expressao { 
	    $$ = novo_no(AST_ARIM_DIVISAO, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
        if ($1->iks_type == IKS_INT || $3->iks_type == IKS_FLOAT)     { $1->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_INT)     { $3->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_INT || $3->iks_type == IKS_BOOL)      { $3->coercao = X_TO_INT; }
	    if ($1->iks_type == IKS_BOOL || $3->iks_type == IKS_INT)      { $1->coercao = X_TO_INT; }
	    if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_BOOL)    { $3->coercao = X_TO_FLOAT; }
	    if ($1->iks_type == IKS_BOOL || $3->iks_type == IKS_FLOAT)    { $1->coercao = X_TO_FLOAT; }
	    
	    $$->tac = tac_exp(AST_ARIM_DIVISAO, $1->tac, $3->tac); 
	}
	| expressao '<' expressao { 
	    $$ = novo_no(AST_LOGICO_COMP_L, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
        if ($1->iks_type == IKS_INT || $3->iks_type == IKS_FLOAT)     { $1->coercao = X_TO_FLOAT; }
        if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_INT)     { $3->coercao = X_TO_FLOAT; }
        
        $$->tac = tac_relop(AST_LOGICO_COMP_L, $1->tac, $3->tac); 
	}
	| expressao '>' expressao { 
	    $$ = novo_no(AST_LOGICO_COMP_G, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
        if ($1->iks_type == IKS_INT || $3->iks_type == IKS_FLOAT)     { $1->coercao = X_TO_FLOAT; }
        if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_INT)     { $3->coercao = X_TO_FLOAT; }   
        
        $$->tac = tac_relop(AST_LOGICO_COMP_G, $1->tac, $3->tac); 
	}
	| expressao TK_OC_LE expressao {
	    $$ = novo_no(AST_LOGICO_COMP_LE, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
        if ($1->iks_type == IKS_INT || $3->iks_type == IKS_FLOAT)     { $1->coercao = X_TO_FLOAT; }
        if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_INT)     { $3->coercao = X_TO_FLOAT; } 
        $$->tac = tac_relop(AST_LOGICO_COMP_LE, $1->tac, $3->tac); 
	     
	}
	| expressao TK_OC_GE expressao { 
	    $$ = novo_no(AST_LOGICO_COMP_GE, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
        if ($1->iks_type == IKS_INT || $3->iks_type == IKS_FLOAT)     { $1->coercao = X_TO_FLOAT; }
        if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_INT)     { $3->coercao = X_TO_FLOAT; }
        
        $$->tac = tac_relop(AST_LOGICO_COMP_GE, $1->tac, $3->tac);   
	}
	| expressao TK_OC_EQ expressao { 
	    $$ = novo_no(AST_LOGICO_COMP_IGUAL, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X; 
        
        $$->tac = tac_relop(AST_LOGICO_COMP_IGUAL, $1->tac, $3->tac); 
	}
	| expressao TK_OC_AND expressao { 
	    $$ = novo_no(AST_LOGICO_E, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
        if ($1->iks_type == IKS_INT || $3->iks_type == IKS_FLOAT)     { $1->coercao = X_TO_FLOAT; }
        if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_INT)     { $3->coercao = X_TO_FLOAT; } 
        
        $$->tac = tac_relop(AST_LOGICO_E, $1->tac, $3->tac);  
	}
	| expressao TK_OC_OR expressao { 
	    $$ = novo_no(AST_LOGICO_OU, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
        if ($1->iks_type == IKS_STRING || $3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
        if ($1->iks_type == IKS_INT || $3->iks_type == IKS_FLOAT)     { $1->coercao = X_TO_FLOAT; }
        if ($1->iks_type == IKS_FLOAT || $3->iks_type == IKS_INT)     { $3->coercao = X_TO_FLOAT; } 
        
        $$->tac = tac_relop(AST_LOGICO_OU, $1->tac, $3->tac);   
	}
	| expressao TK_OC_NE expressao { 
	    $$ = novo_no(AST_LOGICO_COMP_DIF, NULL, 2, $1, $3);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($1->iks_type == IKS_CHAR || $3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;  
	    
	    $$->tac = tac_relop(AST_LOGICO_COMP_DIF, $1->tac, $3->tac);
	}
	  /* Inversão de sinal, exemplo: a = -3; */
	| '-' %prec INVERSAO expressao { 
	    $$ = novo_no(AST_ARIM_INVERSAO, NULL, 1, $2);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($2->iks_type == IKS_CHAR) return IKS_ERROR_CHAR_TO_X;
        if ($2->iks_type == IKS_STRING) return IKS_ERROR_STRING_TO_X; 
	}
	| '!' expressao { 
	    $$ = novo_no(AST_LOGICO_COMP_NEGACAO, NULL, 1, $2);

	    tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

	    if ($2->iks_type == IKS_CHAR) return IKS_ERROR_CHAR_TO_X;
        if ($2->iks_type == IKS_STRING) return IKS_ERROR_STRING_TO_X;
        
        $$->tac = tac_relop(AST_LOGICO_COMP_NEGACAO, $2->tac, NULL); 
	}
	| chamada_funcao { $$ = $1; }
;

shift: 
	identificador TK_OC_SL literal { 
		$$ = novo_no(AST_SHIFT_LEFT, NULL, 2, $1, $3); 

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;

        $$->tac = tac_shift(AST_SHIFT_LEFT, $1->tac, tac_lit(IKS_INT, $3->entrada)); 
	}
	| identificador TK_OC_SR literal { 
		$$ = novo_no(AST_SHIFT_RIGHT, NULL, 2, $1, $3); 

		tree_list *newListNode = (tree_list*) malloc(sizeof(tree_list));
        newListNode->previous = lista_de_nodos;
        newListNode->node = $$;
        lista_de_nodos = newListNode;
        $$->tac = tac_shift(AST_SHIFT_RIGHT, $1->tac,  tac_lit(IKS_INT, $3->entrada)); 
	}
;


%%
