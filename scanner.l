/*
	The Final Compiler Team
	Ana Schwendler
	Lucas Cardozo
*/
%option   yylineno
%{
#include "main.h"
#include "parser.h" //arquivo automaticamente gerado pelo bison
#include "cc_misc.h"

//comp_dict_t* dictionary = NULL;
%}

DIGIT	[0-9]
LETTER_	[A-Za-z_]
%x IN_COMMENT

%%

"int"       {   return TK_PR_INT;       }
"float"     {   return TK_PR_FLOAT;     }
"bool"      {   return TK_PR_BOOL;      }
"char"      {   return TK_PR_CHAR;      }
"string"    {   return TK_PR_STRING;    }
"if"        {   return TK_PR_IF;        }
"then"      {   return TK_PR_THEN;      }
"else"      {   return TK_PR_ELSE;      }
"while"     {   return TK_PR_WHILE;     }
"do"        {   return TK_PR_DO;        }
"input"     {   return TK_PR_INPUT;     }
"output"    {   return TK_PR_OUTPUT;    }
"return"    {   return TK_PR_RETURN;    }
"const"     {   return TK_PR_CONST;     }
"static"    {   return TK_PR_STATIC;    }

"<="	    {	return TK_OC_LE;	    }
">="	    {	return TK_OC_GE;	    }
"=="	    {	return TK_OC_EQ;	    }
"!="	    {	return TK_OC_NE;	    }
"&&"	    {	return TK_OC_AND;	    }
"||"	    {	return TK_OC_OR;	    }
">>"        {	return TK_OC_SR;	    }
"<<"        {	return TK_OC_SL; 	    }

"true"      {             
                comp_dict_entry_t *dict_entry;              
                comp_dict_value_t *newValue;
                char *identifier = strdup(yytext);
                newValue = dict_create_new_value(identifier, yylineno, SIMBOLO_LITERAL_BOOL);
                dict_entry = dict_insert(dictionary, identifier, newValue);                 
                yylval.valor_simbolo_lexico = dict_entry; 
                free(newValue);
                return TK_LIT_TRUE;	
            } 

"false"     {   
                comp_dict_entry_t *dict_entry; 
                comp_dict_value_t *newValue;
                char *identifier = strdup(yytext);
                newValue = dict_create_new_value(identifier, yylineno, SIMBOLO_LITERAL_BOOL);
                dict_entry = dict_insert(dictionary, identifier, newValue);
                yylval.valor_simbolo_lexico = dict_entry;
                free(newValue);
                return TK_LIT_FALSE;    
            }

{DIGIT}+                        {	
                                    comp_dict_entry_t *dict_entry; 
                                    comp_dict_value_t *newValue;
                                    char *identifier = strdup(yytext);
                                    newValue = dict_create_new_value(identifier, yylineno, SIMBOLO_LITERAL_INT);
                                    dict_entry = dict_insert(dictionary, identifier, newValue);
                                    yylval.valor_simbolo_lexico = dict_entry;
                                    free(newValue);
                                    return TK_LIT_INT;		    
                                } 
{DIGIT}+\.{DIGIT}+            	{	
                                    comp_dict_entry_t *dict_entry; 
                                    comp_dict_value_t *newValue;
                                    char *identifier = strdup(yytext);
                                    newValue = dict_create_new_value(identifier, yylineno, SIMBOLO_LITERAL_FLOAT);
                                    dict_entry = dict_insert(dictionary, identifier, newValue);
                                    yylval.valor_simbolo_lexico = dict_entry;
                                    free(newValue);
                                    return TK_LIT_FLOAT;    	
                                }
\'.\'		              	    {	
                                    comp_dict_entry_t *dict_entry; 
                                    comp_dict_value_t *newValue;
                                    char *identifier = strdup(yytext + 1); // +1 Para tirar a primeira aspa simples
                                    identifier[1] = '\0'; // Retira a segunda aspa simples
                                    newValue = dict_create_new_value(identifier, yylineno, SIMBOLO_LITERAL_CHAR);
                                    dict_entry = dict_insert(dictionary, identifier, newValue);
                                    yylval.valor_simbolo_lexico = dict_entry;
                                    free(newValue);
                                    return TK_LIT_CHAR;		    
                                }
\"(\\.|[^"])*\"               	{	
                                    comp_dict_entry_t *dict_entry; 
                                    comp_dict_value_t *newValue;
                                    char *identifier = strdup(yytext + 1); // +1 Para tirar a primeira aspa
                                    char *identifierLast = identifier + strlen(identifier) - 1;
                                    *identifierLast = '\0'; // Retira a segunda aspa
                                    newValue = dict_create_new_value(identifier, yylineno, SIMBOLO_LITERAL_STRING);
                                    dict_entry = dict_insert(dictionary, identifier, newValue);
                                    yylval.valor_simbolo_lexico = dict_entry;
                                    free(newValue);
                                    return TK_LIT_STRING;   	
                                }
{LETTER_}({LETTER_}|{DIGIT})* 	{	
                                    yylval.identificador = strdup(yytext);
                                    return TK_IDENTIFICADOR;	
                                } 

"//".*	    {} /* Remove comentários de uma linha */

<INITIAL>
{
	"/*"      BEGIN(IN_COMMENT);
}
<IN_COMMENT>
{
     	"*/"      BEGIN(INITIAL);
     	[^*\n]+   
     	"*"
     	\n	  
}

[\,\;\:\(\)\[\]\{\}\+\-\*\/\<\>\=\!\&\$] {    return yytext[0];    } /* Reconhece caracteres especiais */

[ \t]+ /* Escapa espaços */

\n  /*Ignora quebra de linha*/

.	{	return TOKEN_ERRO;	}

%%

